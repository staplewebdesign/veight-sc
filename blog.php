<?php
//Template Name: _Blog
?>
<?php get_header(); ?>

<div class="main_content">  

	<div class="content-page">

		<article class="single_page">
			<?php
			rewind_posts();
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args= array(
				'cat' => -13,
				'paged' => $paged
			);
			query_posts($args);
			?>
			<?php if (have_posts()) : ?>
		        <?php while (have_posts()) : the_post(); ?>    
				
						<article>

							<header>
								<p class="post-meta">
									<?php the_time('F jS Y') ?>
								</p>
								<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							</header>
								
								<!-- Display the Post's Content in a div box. -->
								<div class="entry">
									<?php the_post_thumbnail( 'single-post-thumbnail' ); ?>
									<?php
										global $more;
										$more = 0;
										the_content('Read on &rarr;');
										?>
								</div>
								
								<!-- Display a comma separated list of the Post's Categories. -->
								<!-- <p class="postmetadata show-on-phones">Posted in <?php the_category(', '); ?></p>
								
						        <span class="comment-count"><?php comments_popup_link('Leave a comment', '1 Comment', '% Comments'); ?></span>  -->
						        
						</article>

						<hr>

		        <?php endwhile; ?>

		     <?php endif; ?>	

		<?php wp_pagenavi(); ?>

		</article>

	</div>
 
</div>
 
<?php get_footer(); ?>
