<div class="main_content">  

	<div class="content-page">

		<article class="single_page">
			<!-- Start the Loop -->
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article class="page_content">
					<header>
						<p class="post-meta">
							<?php the_time('F jS Y') ?>
						</p>
						<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
					</header>
					<?php the_post_thumbnail( 'single-post-thumbnail' ); ?>
					<section class="entry-content">
						<?php the_content(); ?>
					</section>

					<footer>

					</footer>
				</article>
				<!-- Closes the first div -->

				<?php comments_template( '', true ); ?>
			
			<!-- Stop The Loop (but note the "else:" - see next line). -->
			<?php endwhile; else: ?>
			
				<!-- The very first "if" tested to see if there were any Posts to -->
				<!-- display.  This "else" part tells what do if there weren't any. -->
				<div class="alert-box error">Sorry, the page you requested was not found</div>
			
			<!--End the loop -->
			<?php endif; ?>
			
		</article>