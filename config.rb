# First, require any additional compass plugins installed on your system.
require 'neat-compass'
# require 'breakpoint'


# Toggle this between :development and :production when deploying the CSS to the
# live server. Development mode will retain comments and spacing from the
# original Sass source and adds line numbering comments for easier debugging.
# environment = :production
# environment = :development

# In development, we can turn on the FireSass-compatible debug_info.
#firesass = false
#firesass = true
disable_warnings = true

# Location of the your project's resources.
css_dir         = ""
sass_dir        = "sass"
images_dir      = "images"
javascripts_dir = "js"
