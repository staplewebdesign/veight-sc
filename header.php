<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
  More info: h5bp.com/b/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php wp_title(' '); ?></title>
  <meta name="author" content="humans.txt">

  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/x-icon" />

  <link rel="stylesheet" type="text/css" href="http://cloud.typography.com/805048/712602/css/fonts.css" />

  <!--Facebook Metadata /-->
  <meta property="fb:page_id" content="10151724651572306" />
  <meta property="og:image" content="http://www.staplewebdesign.com/img/og-image.jpg" />
  <meta property="og:description" content="Custom Crafted Web Design and Development"/>
  <meta property="og:title" content="Staple Web Design"/>

  <!--Google+ Metadata /-->
  <meta itemprop="name" content="Staple Web Design">
  <meta itemprop="description" content="Custom Crafted Web Design and Development">
  <meta itemprop="image" content="http://www.staplewebdesign.com/img/og-image.jpg">

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.59911.js"></script>

  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

  <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>


<div id="nav-wrap">

  <figure id="logo-wrap">
    <a href="<?php bloginfo('url'); ?>" id="logo"><img class="logo animated flipInX" src="<?php bloginfo('template_url'); ?>/images/logo.png" /></a>
  </figure>
  <!-- <nav class="head-nav nav-primary" role="navigation">
    <ul class="nav-right">
      <li class="nav-current"><a  href="<?php bloginfo('url'); ?>">Home</a></li>
      <li><a href="<?php bloginfo('url'); ?>/portfolio/">Work</a></li>
      <li><a href="<?php bloginfo('url'); ?>/services/">Services</a></li>
      <li><a href="<?php bloginfo('url'); ?>/blog/">Blog</a></li>
      <li><a href="<?php bloginfo('url'); ?>/about/">About</a></li>
      <li><a href="<?php bloginfo('url'); ?>/contact-swd/">Contact</a></li>
    </ul>
  </nav> -->

  <nav id="menu" class="nav" role="navigation">
    <ul>
      <li>
        <a href="<?php bloginfo('url'); ?>" title="Home">
          <span class="icon"><i aria-hidden="true" class="icon-home"></i></span><span>Home</span>
        </a>
      </li>
      <li>
        <a href="<?php bloginfo('url'); ?>/portfolio/">
          <span class="icon"><i aria-hidden="true" class="icon-briefcase"></i></span><span>Work</span>
        </a>
      </li>
      <li>
        <a href="<?php bloginfo('url'); ?>/blog/">
          <span class="icon"><i aria-hidden="true" class="icon-quote-left"></i></span><span>Blog</span>
        </a>
      </li>
      <li>
        <a href="<?php bloginfo('url'); ?>/about/">
          <span class="icon"><i aria-hidden="true" class="icon-info-sign"></i></span><span>About</span>
        </a>
      </li>
      <li>
        <a href="<?php bloginfo('url'); ?>/contact-swd/">
          <span class="icon"><i aria-hidden="true" class="icon-envelope-alt"></i></span><span>Contact</span>
        </a>
      </li>
    </ul>
  </nav>

</div>

<header id="masthead">

  <div class="container home-hero">

    <?php if ( is_home() ) { ?>
      <div class="home_main">
        <section class="intro">
          <h1>Buffalo Web Design</h1>
          <h4>Staple Web Design is a small design studio building websites that bridge the gap between form and function</h4>
        </section>
        <section class="intro-cta">
          <a class="btn intro-contact animated flipInX" href="<?php bloginfo('url'); ?>/contact-swd/"><span class="icon">&rarr;</span> Let’s Talk</a>
        </section>
      </div>
    <?php } ?>

  </div>

</header>
