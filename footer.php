
</div><!-- #content_container -->

<footer class="main">
  <div class="footer-wrap container">

      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Sidebar')) : ?>
      <?php endif; ?>

      <div class="social_stalking foot-section">
        <h4>Connect with Me:</h4>
          <a class="social-icon" href="http://www.dribbble.com/andystaple"><img src="<?php bloginfo('template_url'); ?>/images/icon-dribbble.svg" alt="Dribbble" /></a>
          <a class="social-icon" href="http://www.twitter.com/andystaple"><img src="<?php bloginfo('template_url'); ?>/images/icon-twitter.svg" alt="Twitter" /></a>
          <a class="social-icon" href="http://www.facebook.com/staplewebdesign"><img src="<?php bloginfo('template_url'); ?>/images/icon-facebook.svg" alt="Facebook" /></a>
          <a class="social-icon" href="https://github.com/AndyStaple"><img src="<?php bloginfo('template_url'); ?>/images/icon-github.svg" alt="Github" /></a>
          <a class="social-icon" href="http://www.linkedin.com/in/andystaple"><img src="<?php bloginfo('template_url'); ?>/images/icon-linkedin.svg" alt="LinkedIn" /></a>
      </div>

      <div class="foot-section">
        <h4>Contact Info</h4>
        <address itemscope itemtype="http://data-vocabulary.org/Organization">
            <span itemprop="name">Staple Web Design</span><br>
            <span itemprop="address" itemscope
              itemtype="http://data-vocabulary.org/Address">
              <span itemprop="street-address">2 Rowland Avenue</span><br>
              <span itemprop="locality">Buffalo</span>, <span itemprop="region">NY</span><br>
            </span><br>
            <span itemprop="tel"><i class="icon-phone"></i> 716-393-9321 (WEB1)</span>
        </address>
      </div>

  </div><!-- #foot_container -->
</footer>


<script type="text/javascript">
//  The function to change the class
var changeClass = function (r,className1,className2) {
    var regex = new RegExp("(?:^|\\s+)" + className1 + "(?:\\s+|$)");
    if( regex.test(r.className) ) {
        r.className = r.className.replace(regex,' '+className2+' ');
    }
    else{
        r.className = r.className.replace(new RegExp("(?:^|\\s+)" + className2 + "(?:\\s+|$)"),' '+className1+' ');
    }
    return r.className;
};

//  Creating our button for smaller screens
var menuElements = document.getElementById('menu');
menuElements.insertAdjacentHTML('afterBegin','<button type="button" id="menutoggle" class="navtoogle" aria-hidden="true"><i aria-hidden="true" class="icon-list-ul"> </i> Menu</button>');

//  Toggle the class on click to show / hide the menu
document.getElementById('menutoggle').onclick = function() {
    changeClass(this, 'navtoogle active', 'navtoogle');
}

// document click to hide the menu
// http://tympanus.net/codrops/2013/05/08/responsive-retina-ready-menu/comment-page-2/#comment-438918
document.onclick = function(e) {
    var mobileButton = document.getElementById('menutoggle'),
        buttonStyle =  mobileButton.currentStyle ? mobileButton.currentStyle.display : getComputedStyle(mobileButton, null).display;

    if(buttonStyle === 'block' && e.target !== mobileButton && new RegExp(' ' + 'active' + ' ').test(' ' + mobileButton.className + ' ')) {
        changeClass(mobileButton, 'navtoogle active', 'navtoogle');
    }
}
</script>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-510739-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/418075.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->


  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/retina.min.js"></script>

  <?php wp_footer(); ?>
</body>
</html>
