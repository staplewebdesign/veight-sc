<?php get_header(); ?>

<div class="main_content">  

	<div class="content-page">

			<article class="single_portfolio">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
				<!-- Begin the first article -->
				<article class="portfolio_row">

					<div id="port_left">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<h3>The Project</h3>
							<?php the_content(); ?>

							<?php $client_testimonial = get_post_meta($post->ID, 'client_testimonial', true);
							//Checking if client testimonial exists
							if ($client_testimonial) { ?>
							<h3>What'd They Think?</h3>
							<blockquote class="quotes"><?php echo $client_testimonial; ?></blockquote>
							<?php }
							else { ?>
							<?php } ?>
					</div>

					<div id="port_right" class="eight columns">
							<img class="port_thumb port_left" src="<?php echo get_post_meta($post->ID, 'image1', true); ?>" alt="" />
							<img class="port_thumb" src="<?php echo get_post_meta($post->ID, 'image2', true); ?>" alt="" />
					</div>
				
				</article>

			<?php endwhile; else: ?>
			
				<!-- The very first "if" tested to see if there were any Posts to -->
				<!-- display.  This "else" part tells what do if there weren't any. -->
				<div class="alert-box error">Sorry, the page you requested was not found</div>
			
			<!--End the loop -->
			<?php endif; ?>

		</article>

	</div>

</div>

<?php get_footer(); ?>