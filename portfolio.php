<?php
/*
Template Name: Portfolio Home
*/
?>

<?php get_header(); ?>

<div class="row page_row">

		<article class="main_content">

			<?php /* If there are no posts to display, such as an empty archive page */ ?>
			<?php if (!have_posts()) : ?>
				<div class="notice">
					<p class="bottom"><?php _e('Sorry, no results were found.'); ?></p>
				</div>
				<?php get_search_form(); ?>
			<?php endif; ?>


			<?php /* Start loop */ ?>
			<?php query_posts('showposts=12&cat=13'); ?>
			<?php while (have_posts()) : the_post(); ?>

			<div class="row portfolio_wrap">
				<article id="post-<?php the_ID(); ?>" <?php post_class('portfolio_loop'); ?>>
					<header>
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					</header>
					<section class="entry-content">
						<?php the_content( '&nbsp;Read More <i class="icon-chevron-right"></i>'); ?>
						<p><a class="view_project" href="<?php the_permalink(); ?>">View Project &rarr;</a></p>
					</section>
				</article><!-- post -->

				<figure class="portfolio_right">
					<?php the_post_thumbnail( 'portfolio-showcase' ); ?>
				</figure>
			</div>

			<?php endwhile; // End the loop ?>


		</article>

</div>

<?php get_footer(); ?>
