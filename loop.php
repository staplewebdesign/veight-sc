<div class="main_content">  

	<div class="content-page">

		<article class="archive_page single_page">

			<?php /* If there are no posts to display, such as an empty archive page */ ?>
			<?php if (!have_posts()) : ?>
				<div class="notice">
					<p class="bottom"><?php _e('Sorry, no results were found.'); ?></p>
				</div>
				<?php get_search_form(); ?>	
			<?php endif; ?>


		<?php /* Start loop */ ?>
		<?php while (have_posts()) : the_post(); ?>
		
			<article id="post-<?php the_ID(); ?>" <?php post_class('excerpt_content'); ?>>
				<header>
					<p class="post-meta">
						<?php the_time('F jS Y') ?>
					</p>
					<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				</header>
				<?php the_post_thumbnail( 'single-post-thumbnail' ); ?>
				<section class="entry-content">
					<?php if (is_archive() || is_search()) : // Only display excerpts for archives and search ?>
						<?php the_excerpt(); ?>
					<?php else : ?>
						<?php the_content( '&nbsp;Read More <i class="icon-chevron-right"></i>'); ?>
					<?php endif; ?>
				</section>
				<footer>
					<?php $tag = get_the_tags(); if (!$tag) { } else { ?><p><i class="icon-tags"></i> <?php the_tags(); ?></p><?php } ?>
				</footer>
			</article><!-- post -->

		<?php endwhile; // End the loop ?>


		<?php /* Display navigation to next/previous pages when applicable */ ?>
		<?php if ($wp_query->max_num_pages > 1) : ?>
			<?php if(function_exists('wp_paginate')) {
			    wp_paginate();
			} ?>
		<?php endif; ?>

		</article>
