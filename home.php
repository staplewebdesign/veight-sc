<?php get_header(); ?>

<div class="main_content">

	<div class="services container">

		<section class="service">
			<img src="<?php bloginfo('template_url'); ?>/images/responsive-design.png" />
			<h2>Web Design</h2>
			<p>Custom design-and-build websites, for people who value the use of design to solve problems.</p>
		</section>

		<section class="service">
			<img src="<?php bloginfo('template_url'); ?>/images/wp-design.png" />
			<h2>WordPress Design</h2>
			<p>I&rsquo;ve been working with WordPress for over 9 years, and am very involved in the WordPress community.</p>
		</section>

		<section class="service">
			<img src="<?php bloginfo('template_url'); ?>/images/app-design.png" />
			<h2>App/UI Design</h2>
			<p>A fine line between function and form is needed to make everything feel right for users.</p>
		</section>

	</div>

</div><!-- .main_content -->

<div class="secondary_content">

	<div class="portfolio_preview">

			<h3>Selected Works</h3>

				<ul class="container">
					<?php query_posts('showposts=3&cat=13'); ?>
					<?php while (have_posts()) : the_post(); ?>
		
						<li class="portfolio_piece align-center">

							<a class="port_link" href="<?php the_permalink() ?>"><?php the_post_thumbnail(); ?></a>
							<div class="piece_title"><?php the_title(); ?><br><span class="year"><?php the_time('Y') ?></span></div>

						</li><!-- .port_wrap -->
					
					<?php endwhile; ?>
				</ul>

			<div class="twelve columns align-center">
				<a class="read-more btn" href="<?php bloginfo('url'); ?>/project-planner/">Start Your Own Project →</a>
			</div>
		</div>
	
</div>

<div class="tertiary_content">
	
	<div class="container">

		<section class="blog_preview">
			<h2>My Design Blog</h2>
			<ul>
			<?php query_posts( 'showposts=2&cat=-13' ); ?>
			<?php while (have_posts()) : the_post(); ?>

				<li>
					<p class="meta-data"><?php the_time('M jS, Y') ?></p>
					<h4 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?> &rarr;</a></h4>
					<?php the_excerpt(); ?>
				</li>
						
			<?php endwhile; ?>
			</ul>
			<?php rewind_posts(); ?>
		</section>
		
		<section class="about_me">
			<h2>About Me and My Design</h2>
			<p class="intro">I have over 8 years of web design and front-end development experience for many different types of businesses.</p> 
			<p>You probably have enough to worry about with your business, let me take care of the website&mdash;it is very often the first impression of your company, make it a good and lasting one. <a class="read-more" href="<?php bloginfo('url'); ?>/about/">read more &rarr;</a></p>
		</section>

	</div>

</div>


<div class="secondary_content">

	<div class="container testimonials">
			<aside class="highlighted">
				<h3>What Others are Saying</h3>
				<q>
					I’ve consistently been blown away
				</q>
				<br>
				<cite>Colin Davis - GetTavern</cite>
			</aside>
			<div class="right-testimonials">
				<blockquote class="highlighted">
					<p>I’ve worked with Staple Web Design for several projects, and I’ve consistently been blown away &hellip; In each instance, Andy and his team have worked with me to find the core of what I&lsquo;m trying to accomplish and built a website that both engages the user and is beautiful to the eye &hellip; I’d certainly come to Staple Web Design as my first stop for any upcoming projects.</p>
				</blockquote>
			</div>
	</div>

</div>

<?php get_footer(); ?>