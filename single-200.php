<?php get_header(); ?>

	<!-- Start the Loop -->
	<?php query_posts(); ?> 
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
		<!-- Begin the first article -->
		<article class="row">

			<div id="port_left" class="four columns">
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php the_content(); ?>
					<p><a href="<?php echo get_post_meta($post->ID, "website", true); ?>" target="_blank">Visit Site</a></p>
					<h5>What We Did</h5>
					<p class="tags"><?php the_tags('<ul id="whatwedid"><li>','</li><li>','</li></ul>'); ?></p>
			</div>

			<div id="port_right" class="eight columns">
				<?php the_post_thumbnail( 'single-post-thumbnail' ); ?>
					<img class="port_thumb port_left" src="<?php echo get_post_meta($post->ID, 'image1', true); ?>" alt="" />
					<img class="port_thumb" src="<?php echo get_post_meta($post->ID, 'image2', true); ?>" alt="" />
			</div>
		
		</article>

<?php get_footer(); ?>