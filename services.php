<?php
//Template Name: Services
?>
<?php get_header(); ?>

<div class="row page_row">  
		
		<!-- Begin the first article -->
		<article class="services row">

			<div class="six columns">
				<div class="two columns">
					<img class="service" src="<?php bloginfo('template_url'); ?>/images/service_design.png" alt="Web Design &amp; Development" />
				</div>
				<div class="ten columns">
					<h2>Web Design</h2>
					<p>From simple to complex, I work with clients of all types to bring together great websites. I have over 8 years of experience with HTML & CSS. A Website is quite often a persons first look at your business and often judge you or your business based on it. Building a high quality, aesthetic and functional site is one of the best ways to have people notice and keep you in high regard.</p>
						<ul>
							<li>Responsive Web Design</li>
							<li>User Interface Design</li>
							<li>Research &amp; Testing</li>
							<li>SEO &amp; Auditing</li>
						</ul>
					<p><a class="button small orange round" href="/services/web-design/">More Info &rarr;</a></p>
				</div>
			</div>

			<div class="six columns">
				<div class="two columns">
					<img class="service" src="<?php bloginfo('template_url'); ?>/images/service_wp.png" alt="WordPress Design &amp; Development" />
				</div>
				<div class="ten columns">
					<h2>WordPress Design</h2>
					<p>Building with WordPress is a great way to have a website, while keeping control of your content. I've been working with WordPress for over 6 years, and even co-organize a bi-weekly meetup in Buffalo, NY discussing anything and everything about it.</p>
						<ul>
							<li>Custom-Built Themes</li>
							<li>Responsive Themes</li>
							<li>Widgetized Areas &amp; Options</li>
							<li>WordPress SEO &amp; Auditing</li>
						</ul>
					<p><a class="button small orange round" href="/services/wordpress-design/">More Info &rarr;</a></p>
				</div>
			</div>
		
		</article>

		<hr>

		<article class="services row">

			<div class="six columns">
				<div class="two columns">
					<img class="service" src="<?php bloginfo('template_url'); ?>/images/service_identity.png" alt="Identity &amp; Branding" />
				</div>
				<div class="ten columns">
					<h2>Identity &amp; Branding</h2>
					<p>A good brand identity will conceptualize what the company stands for, and of course look good doing it.</p>
						<ul>
							<li>Logo Design</li>
							<li>Brand Styleguide</li>
							<li>Vector Format</li>
						</ul>
					<p><a class="button small orange round" href="/services/brand-identity-design/">More Info &rarr;</a></p>
				</div>
			</div>

			<div class="six columns">
				<div class="two columns">
					<img class="service" src="<?php bloginfo('template_url'); ?>/images/service_strategy.png" alt="Web Strategy &amp; Consultation" />
				</div>
				<div class="ten columns">
					<h2>Web Strategy</h2>
					<p>Get some help in making sure your online presense is saying what you want it to, and help along the way.</p>
					<p><a class="button small orange round" href="/services/web-design/">More Info &rarr;</a></p>
				</div>
			</div>

		</article>

<?php get_footer(); ?>
